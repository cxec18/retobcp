package com.example.springboot;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint, Serializable {
	
	private static final long serialVersionUID = -7858869558953243875L;
	
	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException {
		

        Map<String ,String > rsp =new HashMap<>();  
        ObjectMapper objectMapper = new ObjectMapper();
        String errorMsg = (String) request.getAttribute("errorMsg");

		System.out.println("*********** JwtAuthenticationEntryPoint: " + errorMsg);
		
        rsp.put("timestamp", new Date().toString()) ;
		rsp.put("status", HttpServletResponse.SC_UNAUTHORIZED + "") ;
		rsp.put("error",  "Unauthorized") ;
        rsp.put("message", errorMsg) ;

   		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.setContentType("application/json;charset=UTF-8");
		response.getWriter().write(objectMapper.writeValueAsString(rsp));
        response.getWriter().flush();
		response.getWriter().close();
			
	}
}
