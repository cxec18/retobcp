package com.example.springboot.filter;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.example.springboot.service.impl.JwtAuthenticationServiceImpl;
import com.example.springboot.util.Constantes;
import com.example.springboot.util.JwtUtil;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;

@Component
public class JwtAythenticationFilter extends OncePerRequestFilter{
	
	@Autowired
    private JwtAuthenticationServiceImpl jwtAuthenticationService;

    @Autowired
    private JwtUtil jwtUtil;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {
    	
    		final String authorizationHeader = request.getHeader("Authorization");

            String username = null;
            String jwt = null;

            System.out.println("username: " + username);
            System.out.println("authorizationHeader: " + authorizationHeader);
            
            if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            	jwt = authorizationHeader.substring(7);
    			try {
    				username = jwtUtil.extractUsername(jwt);
    				request.setAttribute("username", username);
    			} catch (MalformedJwtException e) {
    				request.setAttribute("errorMsg", "MalformedJwtException: Unable to get JWT Token");
    			}catch (IllegalArgumentException e) {
    				request.setAttribute("errorMsg", "IllegalArgumentException: " + e.getMessage());
    			} catch (ExpiredJwtException e) {
    				request.setAttribute("errorMsg", "ExpiredJwtException: " + e.getMessage());
    			}catch (Exception e) {
    				request.setAttribute("errorMsg", "Exception: " + e.getMessage());
    			}
    		} else {
				request.setAttribute("errorMsg", "JWT Token does not begin with Bearer String");
    		}

    		if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {

				UserDetails userDetails = this.jwtAuthenticationService.loadUserByUsername(username);
                
    			if (jwtUtil.validateToken(jwt, userDetails)) {

    				UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
    						userDetails, null, userDetails.getAuthorities());
    				usernamePasswordAuthenticationToken
    						.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
    				SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
    			}
    		}
    		chain.doFilter(request, response);

    }
    
    private static final List<String> EXCLUDE_URL = Arrays.asList(Constantes.AUTENTICACION);
    
    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) {
        boolean isAuthenticationReq = EXCLUDE_URL.stream().anyMatch(exclude -> exclude.equalsIgnoreCase(request.getServletPath()));
        return isAuthenticationReq;

    }
}
