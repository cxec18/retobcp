package com.example.springboot.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.springboot.domain.Usuario;

@Service
public interface UsuarioService {

	public Usuario findByUsername(String username);

	public List<Usuario> findAllActivos();
}
