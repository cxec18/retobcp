package com.example.springboot.service.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.example.springboot.domain.Usuario;
import com.example.springboot.service.UsuarioService;

@Component
@Transactional
public class JwtAuthenticationServiceImpl implements UserDetailsService{

	@Autowired
	private UsuarioService usuarioService;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		Usuario us  = usuarioService.findByUsername(username);
		
		System.out.println("us: " + us);
		
		if(us!=null) {
			System.out.println("us.getUsername: " + us.getUsername());
			System.out.println("us.getPassword: " + us.getPassword());
			return new User(us.getUsername(), us.getPassword(), new ArrayList<>());
		}else {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
		
	}
	
	

}
