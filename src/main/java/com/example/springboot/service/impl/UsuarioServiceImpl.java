package com.example.springboot.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.springboot.domain.Usuario;
import com.example.springboot.repositories.UsuarioRepository;
import com.example.springboot.service.UsuarioService;

@Service
@Transactional
public class UsuarioServiceImpl implements UsuarioService{
	
	@Autowired
	private UsuarioRepository usuarioRepository;

	public Usuario findByUsername(String username){
		return usuarioRepository.customFindByUsername(username);
	}

	public List<Usuario> findAllActivos(){
		return usuarioRepository.customFindAllActivos();
	}


}
