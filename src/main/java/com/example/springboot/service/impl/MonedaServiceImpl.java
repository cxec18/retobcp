package com.example.springboot.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.springboot.domain.Moneda;
import com.example.springboot.repositories.MonedaRepository;
import com.example.springboot.service.MonedaService;

@Service
@Transactional
public class MonedaServiceImpl implements MonedaService {
	
	@Autowired
	private MonedaRepository monedaRepository;

	public Moneda findBySimbolo(String username){
		return monedaRepository.customFindBySimbolo(username);
	}

	public List<Moneda> findAllActivos(){
		return monedaRepository.customFindAllActivos();
	}

}
