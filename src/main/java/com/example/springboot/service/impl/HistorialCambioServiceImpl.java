package com.example.springboot.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.springboot.domain.HistorialCambio;
import com.example.springboot.domain.TipoCambio;
import com.example.springboot.repositories.HistorialCambioRepository;
import com.example.springboot.service.HistorialCambioService;
import com.example.springboot.util.Constantes;

@Service
@Transactional
public class HistorialCambioServiceImpl implements HistorialCambioService {
	
	@Autowired
	private HistorialCambioRepository historialCambioRepository;

	@Autowired
	private TipoCambioServiceImpl tipoCambioService;

	public List<HistorialCambio> findByIdTipoCambio(Long tipoCambioKey){
		return historialCambioRepository.customFindByIdTipoCambio(tipoCambioKey);
	}

	public List<HistorialCambio> findAll(){
//		List<HistorialCambio> lista = historialCambioRepository.customFindAll();
		Iterable<HistorialCambio> lista = (List<HistorialCambio>) historialCambioRepository.findAll();
		for(HistorialCambio hc: lista) {
			System.out.println("hck: " + hc.getHistorialCambioKey() + "tpk: " + hc.getTipoCambio().getTipoCambioKey());
		}
		return (List<HistorialCambio>) lista;
	}

	public List<HistorialCambio> findAllV2(){

		List<HistorialCambio> listaHC = (List<HistorialCambio>) historialCambioRepository.customFindAllV2();
		List<TipoCambio> listaTC = tipoCambioService.findAll();
		
		List<HistorialCambio> nlistaHC = new ArrayList<HistorialCambio>();
		
		for(HistorialCambio hc: listaHC) {
			TipoCambio tp = listaTC.stream()
					  .filter(customer ->  hc.getTipoCambioKey().equals(customer.getTipoCambioKey()))
					  .findAny()
					  .orElse(null);
			
			hc.setTipoCambio(tp);
			nlistaHC.add(hc);
		}
		return (List<HistorialCambio>) nlistaHC;
	}
	
	public HistorialCambio calculate(HistorialCambio historialCambio, HttpServletRequest request) throws Exception {

		//RECUPERA USUARIO
		String username = (String) request.getAttribute("username");
		
		//OBTENER 1 TIPO DE CAMBIO SU ID
		TipoCambio tipoCambio = tipoCambioService.findById(historialCambio.getTipoCambio().getTipoCambioKey());

		// SI EXISTE, SE CAMBIA DE ESTADO AL EXISTENTE
		if(tipoCambio!=null) {
			
			Double monedaDestino = 0D;
			Date fechaRegistro = new Date();
			
			if (historialCambio.getValorMonedaOrigen()!=null){
				if(tipoCambio.getTasaConversion()!=null && tipoCambio.getTasaConversion()>0) {
					monedaDestino = historialCambio.getValorMonedaOrigen() * tipoCambio.getTasaConversion();
				}else {
					throw new Exception(Constantes.MSG_TASA_INCORRECTA);
				}
			} else {
				throw new Exception(Constantes.MSG_BODY_INCORRECTO);
			}
			
			HistorialCambio nHistorialCambio = new HistorialCambio();
			nHistorialCambio.setUsername(username);
			nHistorialCambio.setValorMonedaOrigen(historialCambio.getValorMonedaOrigen());
			nHistorialCambio.setValorMonedaDestino(monedaDestino);
			nHistorialCambio.setFecha(fechaRegistro);
			
			nHistorialCambio.setTipoCambio(tipoCambio); // Añadir Tipo de cambio
			historialCambio = save(nHistorialCambio);
		} else {
			throw new Exception(Constantes.MSG_REGISTRO_NO_ENCONTRADO);
		}
				
		return historialCambio;
	}
	

	
	private HistorialCambio save(HistorialCambio historialCambio) {

		Long idGenerated = historialCambioRepository.getNextSeriesId();
		
		historialCambio.setHistorialCambioKey(idGenerated);
		
		try {
			historialCambioRepository.customSave(historialCambio.getHistorialCambioKey(),
					historialCambio.getTipoCambio().getTipoCambioKey(),
					historialCambio.getValorMonedaOrigen(),
					historialCambio.getValorMonedaDestino(),
					historialCambio.getUsername(),
					historialCambio.getFecha());
		}catch (Exception e) {
			e.printStackTrace();
		}

		return historialCambio;
	}


}
