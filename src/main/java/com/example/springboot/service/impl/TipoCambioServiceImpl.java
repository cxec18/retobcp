package com.example.springboot.service.impl;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.springboot.domain.Moneda;
import com.example.springboot.domain.TipoCambio;
import com.example.springboot.repositories.TipoCambioRepository;
import com.example.springboot.service.MonedaService;
import com.example.springboot.service.TipoCambioService;
import com.example.springboot.util.Constantes;

@Service
@Transactional
public class TipoCambioServiceImpl implements TipoCambioService {
	
	@Autowired
	private TipoCambioRepository tipoCambioRepository;

	@Autowired
	private MonedaService monedaService;

	@Override
	public TipoCambio findById(Long id){
		return tipoCambioRepository.customFindById(id);
	}

	@Override
	public List<TipoCambio> findAllActivos(){
		return tipoCambioRepository.customFindAllActivos();
	}
	
	@Override
	public TipoCambio findByOrigenAndDestino(String origen, String destino) {
		return tipoCambioRepository.customFindByOrigenAndDestino(origen, destino);
	}


	@Override
	public List<TipoCambio> findAll(){ 
		return (List<TipoCambio>) tipoCambioRepository.findAll();
	}
	
	@Override
	public TipoCambio saveLogic(TipoCambio tipoCambio, HttpServletRequest request) throws Exception {

		// VALIDA EL BODY DEL REQUEST
		if(tipoCambio.getSimboloMonedaOrigen()==null || tipoCambio.getSimboloMonedaDestino	()==null ||
				tipoCambio.getTasaConversion()==null) {
			throw new Exception(Constantes.MSG_BODY_INCORRECTO);
		}

		// VALIDACIONES EXTRA
		if(tipoCambio.getTasaConversion()<=0L) {
			throw new Exception(Constantes.MSG_TASA_INCORRECTA);
		}

		//RECUPERA USUARIO
		String username = (String) request.getAttribute("username");

		Moneda monedaOrigen = monedaService.findBySimbolo(tipoCambio.getSimboloMonedaOrigen());
		Moneda monedaDestino = monedaService.findBySimbolo(tipoCambio.getSimboloMonedaDestino());
		
		if(monedaOrigen==null) {
			throw new Exception(Constantes.MSG_MONEDA_ORIGEN_INCORRECTO);
		}
		if(monedaDestino==null) {
			throw new Exception(Constantes.MSG_MONEDA_DESTINO_INCORRECTO);
		}
		
		//OBTENER 1 POR ORIGEN Y DESTINO
		TipoCambio fTipoCambio = tipoCambioRepository.customFindByOrigenAndDestino(monedaOrigen.getSimbolo(), monedaDestino.getSimbolo());
		
		// SI EXISTE, SE CAMBIA DE ESTADO AL EXISTENTE
		if(fTipoCambio!=null) {
			fTipoCambio.setUsername(username);    // REGISTRA ULTIMO USUARIO QUE ACTUALIZA
			fTipoCambio.setActivo(false);         // INHABILITA EL REGISTRO ANTERIOR
			update(fTipoCambio);
		}

		Date fechaRegistro = new Date();
		
		//SE REGISTRA EL NUEVO 'TIPO DE CAMBIO'
		tipoCambio.setUsername(username);
		tipoCambio.setFecha(fechaRegistro);
		tipoCambio.setActivo(true);
		tipoCambio = save(tipoCambio);

		return tipoCambio;
	}
	
	@Override
	public TipoCambio updateLogic(Long key, TipoCambio tipoCambio, HttpServletRequest request) throws Exception {

		// VALIDA EL BODY DEL REQUEST
		if(tipoCambio.getTasaConversion()==null) {
			throw new Exception(Constantes.MSG_BODY_INCORRECTO);
		}

		// VALIDACIONES EXTRA
		if(tipoCambio.getTasaConversion()<=0L) {
			throw new Exception(Constantes.MSG_TASA_INCORRECTA);
		}

		//RECUPERA USUARIO
		String username = (String) request.getAttribute("username");
		
		//OBTENER 1 POR ID
		TipoCambio fTipoCambio = tipoCambioRepository.customFindById(key);
		
		// SI EXISTE
		if(fTipoCambio!=null && key.longValue()==fTipoCambio.getTipoCambioKey().longValue()) {

			if(tipoCambio.getSimboloMonedaOrigen()!=null) {
				fTipoCambio.setSimboloMonedaOrigen(tipoCambio.getSimboloMonedaOrigen());
			}
			if(tipoCambio.getSimboloMonedaDestino()!=null) {
				fTipoCambio.setSimboloMonedaDestino(tipoCambio.getSimboloMonedaDestino());
			}
			if(tipoCambio.getTasaConversion()!=null) {
				fTipoCambio.setTasaConversion(tipoCambio.getTasaConversion());
			}
			fTipoCambio.setUsername(username);
			
			update(fTipoCambio);

		}else {
			throw new Exception(Constantes.MSG_REGISTRO_NO_ENCONTRADO);
		}
				
		return fTipoCambio;
	}

	@Override
	public void deleteLogic(Long id, HttpServletRequest request) throws Exception {

		//RECUPERA USUARIO
		String username = (String) request.getAttribute("username");
		
		//OBTENER 1 POR ID
		TipoCambio fTipoCambio = tipoCambioRepository.customFindById(id);
		
		// SI EXISTE
		if(fTipoCambio!=null && id.longValue()==fTipoCambio.getTipoCambioKey().longValue()) {
			fTipoCambio.setUsername(username);
			fTipoCambio.setActivo(false);
			update(fTipoCambio);
		}else {
			throw new Exception(Constantes.MSG_REGISTRO_NO_ENCONTRADO);
		}
	}
	
	
	private TipoCambio save(TipoCambio tipoCambio) {

		Long idGenerated = tipoCambioRepository.getNextSeriesId();
		
		tipoCambio.setTipoCambioKey(idGenerated);
		
		try {
			tipoCambioRepository.customSave(tipoCambio.getTipoCambioKey(),
					tipoCambio.getSimboloMonedaOrigen(),
					tipoCambio.getSimboloMonedaDestino(),
					tipoCambio.getTasaConversion(),
					tipoCambio.getUsername(),
					tipoCambio.getFecha());
		}catch (Exception e) {
			e.printStackTrace();
		}

		
		return tipoCambio;
	}
	
	private TipoCambio update(TipoCambio tipoCambio) {
		
		Date fechaRegistro = new Date();
		tipoCambio.setFecha(fechaRegistro);
		
		tipoCambioRepository.customUpdate(tipoCambio.getTipoCambioKey(),
				tipoCambio.getSimboloMonedaOrigen(),
				tipoCambio.getSimboloMonedaDestino(),
				tipoCambio.getTasaConversion(),
				tipoCambio.getUsername(),
				tipoCambio.getFecha(),
				tipoCambio.getActivo());
		return tipoCambio;
	}

}
