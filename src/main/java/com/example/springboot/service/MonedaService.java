package com.example.springboot.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.springboot.domain.Moneda;

@Service
public interface MonedaService {

	List<Moneda> findAllActivos();

	Moneda findBySimbolo(String simbolo);

}
