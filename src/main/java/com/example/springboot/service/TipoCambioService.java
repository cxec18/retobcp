package com.example.springboot.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import com.example.springboot.domain.TipoCambio;

@Service
public interface TipoCambioService {

	TipoCambio findById(Long id);
	
	List<TipoCambio> findAll();
	
	List<TipoCambio> findAllActivos();

	TipoCambio findByOrigenAndDestino(String origen, String destino);

	TipoCambio saveLogic(TipoCambio tipoCambio, HttpServletRequest request) throws Exception;

	TipoCambio updateLogic(Long id, TipoCambio tipoCambio, HttpServletRequest request) throws Exception ;

	void deleteLogic(Long id, HttpServletRequest request) throws Exception ;

}
