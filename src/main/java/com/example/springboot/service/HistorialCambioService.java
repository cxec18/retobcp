package com.example.springboot.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import com.example.springboot.domain.HistorialCambio;

@Service
public interface HistorialCambioService {

	List<HistorialCambio> findAll();

	List<HistorialCambio> findAllV2();

	HistorialCambio calculate(HistorialCambio historialCambio, HttpServletRequest request) throws Exception;

}
