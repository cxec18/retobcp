package com.example.springboot;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
@EntityScan("com.example.springboot.domain")
public class Application {

	@Autowired
	private Environment env;
 
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	@Bean
	@Primary
	public DataSource firstDataSource() {
		return DataSourceBuilder.create()
				.url(env.getProperty("spring.datasource.url"))
				.password(env.getProperty("spring.datasource.password"))
				.username(env.getProperty("spring.datasource.username"))
				.driverClassName(env.getProperty("spring.datasource.driverClassName"))
				.build();
	}
	
}
