package com.example.springboot.models;

import org.springframework.http.HttpStatus;

public class OkResponse {

    private HttpStatus status;
    private String message;

    public OkResponse(String message) {
        super();
        this.status = HttpStatus.OK;
        this.message = message;
    }

	public HttpStatus getStatus() {
		return status;
	}
	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}    
    
}
