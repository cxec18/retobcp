package com.example.springboot.models;

public class UsuarioResponse {

	private final String jwt;
	
	public UsuarioResponse(String jwt){
		this.jwt = jwt;
	}
	
	public String getJwt() {
		return jwt;
	}
}
