package com.example.springboot.domain;

import java.io.Serializable;

import org.springframework.data.annotation.Id;

import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Table
public class Moneda implements Serializable{

    private static final long serialVersionUID = -1375405007868923427L;
    
	@Id
    @Column("SIMBOLO")
    private String simbolo;

    @Column("NOMBRE")
    private String nombre;

    @Column("ACTIVO")
    private Boolean activo;

    
	public String getSimbolo() {
		return simbolo;
	}
	public void setSimbolo(String simbolo) {
		this.simbolo = simbolo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Boolean isActivo() {
		return activo;
	}
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}
    
}
