package com.example.springboot.domain;


import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Table
public class Usuario implements Serializable{

    private static final long serialVersionUID = 6396336405158170608L;
    
	@Id
    @Column("USERNAME")
    private String username;

    @Column("PASSWORD")
    private String password;

    @Column("ROL")
    private String rol;

    @Column("ACTIVO")
    private Boolean activo;

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRol() {
		return rol;
	}
	public void setRol(String rol) {
		this.rol = rol;
	}
	public Boolean isActivo() {
		return activo;
	}
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

    
}