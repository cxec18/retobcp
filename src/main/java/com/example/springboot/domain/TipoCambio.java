package com.example.springboot.domain;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Table
public class TipoCambio implements Serializable{

    private static final long serialVersionUID = -3375405007868923427L;
    
	@Id
    @Column("TIPO_CAMBIO_KEY")
    private Long tipoCambioKey;

    @Column("SIMBOLO_MONEDA_ORIGEN")
    private String simboloMonedaOrigen;

    @Column("SIMBOLO_MONEDA_DESTINO")
    private String simboloMonedaDestino;
    
    @Column("TASA_CONVERSION")
    private Double tasaConversion;

    @Column("USERNAME")
    private String username;

    @Column("FECHA")
    private Date fecha;
    
    @Column("ACTIVO")
    private Boolean activo;

	public Long getTipoCambioKey() {
		return tipoCambioKey;
	}
	public void setTipoCambioKey(Long tipoCambioKey) {
		this.tipoCambioKey = tipoCambioKey;
	}
	public String getSimboloMonedaOrigen() {
		return simboloMonedaOrigen;
	}
	public void setSimboloMonedaOrigen(String simboloMonedaOrigen) {
		this.simboloMonedaOrigen = simboloMonedaOrigen;
	}
	public String getSimboloMonedaDestino() {
		return simboloMonedaDestino;
	}
	public void setSimboloMonedaDestino(String simboloMonedaDestino) {
		this.simboloMonedaDestino = simboloMonedaDestino;
	}
	public Double getTasaConversion() {
		return tasaConversion;
	}
	public void setTasaConversion(Double tasaConversion) {
		this.tasaConversion = tasaConversion;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public Boolean getActivo() {
		return activo;
	}
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

}
