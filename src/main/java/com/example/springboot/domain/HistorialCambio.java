package com.example.springboot.domain;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Table
public class HistorialCambio implements Serializable{

    private static final long serialVersionUID = -4928557296423893476L;
    
	@Id
    @Column("HISTORIAL_CAMBIO_KEY")
    private Long historialCambioKey;

    @Column("VALOR_MONEDA_ORIGEN")
    private Double valorMonedaOrigen;
    
    @Column("VALOR_MONEDA_DESTINO")
    private Double valorMonedaDestino;

    @Column("FECHA")
    private Date fecha;

    @Column("USERNAME")
    private String username;

    @Column("TIPO_CAMBIO_KEY")
    @JsonIgnore
    private Long tipoCambioKey;
    
    @Column("TIPO_CAMBIO_KEY")
    private TipoCambio tipoCambio;
    
	public Long getHistorialCambioKey() {
		return historialCambioKey;
	}
	public void setHistorialCambioKey(Long historialCambioKey) {
		this.historialCambioKey = historialCambioKey;
	}
	public Double getValorMonedaOrigen() {
		return valorMonedaOrigen;
	}
	public void setValorMonedaOrigen(Double valorMonedaOrigen) {
		this.valorMonedaOrigen = valorMonedaOrigen;
	}
	public Double getValorMonedaDestino() {
		return valorMonedaDestino;
	}
	public void setValorMonedaDestino(Double valorMonedaDestino) {
		this.valorMonedaDestino = valorMonedaDestino;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public Long getTipoCambioKey() {
		return tipoCambioKey;
	}
	public void setTipoCambioKey(Long tipoCambioKey) {
		this.tipoCambioKey = tipoCambioKey;
	}
	public TipoCambio getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(TipoCambio tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

	
}
