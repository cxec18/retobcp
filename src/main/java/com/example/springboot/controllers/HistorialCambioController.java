package com.example.springboot.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.springboot.domain.HistorialCambio;
import com.example.springboot.models.ErrorResponse;
import com.example.springboot.service.HistorialCambioService;
import com.example.springboot.util.Constantes;

@RestController
@RequestMapping(Constantes.HISTORIAL_TIPO_CAMBIO)
public class HistorialCambioController {

	@Autowired
	private HistorialCambioService historialCambioService;

	@GetMapping(Constantes.HISTORIALES_DE_TIPOS_DE_CAMBIO)
    public ResponseEntity<List<HistorialCambio>> historialCambio(HttpServletRequest request){
		List<HistorialCambio> tiposCambio = historialCambioService.findAll();
        return new ResponseEntity<List<HistorialCambio>>(tiposCambio, HttpStatus.OK);
    }
	@GetMapping(Constantes.HISTORIALES_DE_TIPOS_DE_CAMBIO_V2)
    public ResponseEntity<List<HistorialCambio>> historialCambioV2(HttpServletRequest request){
		List<HistorialCambio> tiposCambio = historialCambioService.findAllV2();
        return new ResponseEntity<List<HistorialCambio>>(tiposCambio, HttpStatus.OK);
    }
	
	@PostMapping(Constantes.HISTORIALES_DE_TIPOS_DE_CAMBIO)
	public ResponseEntity<?> create(@RequestBody HistorialCambio historialCambio, HttpServletRequest request) throws Exception {
		try {
			HistorialCambio historialCambioResponse = historialCambioService.calculate(historialCambio, request);
			return new ResponseEntity<HistorialCambio>(historialCambioResponse, HttpStatus.OK);
		}catch (Exception e) {
			ErrorResponse ErrorResponse = new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getLocalizedMessage());
			return new ResponseEntity<ErrorResponse>(ErrorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
}
