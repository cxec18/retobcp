package com.example.springboot.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.springboot.domain.TipoCambio;
import com.example.springboot.models.ErrorResponse;
import com.example.springboot.models.OkResponse;
import com.example.springboot.service.TipoCambioService;
import com.example.springboot.util.Constantes;


@RestController
@RequestMapping(Constantes.TIPO_CAMBIO)
public class TipoCambioController {

	@Autowired
	private TipoCambioService tipoCambioService;

	@GetMapping(Constantes.TIPOS_DE_CAMBIO)
    public ResponseEntity<List<TipoCambio>> tipoCambio(HttpServletRequest request){
		List<TipoCambio> tiposCambio = tipoCambioService.findAllActivos();
        return new ResponseEntity<List<TipoCambio>>(tiposCambio, HttpStatus.OK);
    }
	
	@GetMapping(Constantes.TIPOS_DE_CAMBIO + Constantes.TIPO_CAMBIO_ORIGEN_DESTINO)
    public ResponseEntity<TipoCambio> tipoCambio(@PathVariable("origen") String origen, @PathVariable("destino") String destino, HttpServletRequest request){
        return new ResponseEntity<TipoCambio>(tipoCambioService.findByOrigenAndDestino(origen, destino), HttpStatus.OK);
    }
	
	@PostMapping(Constantes.TIPOS_DE_CAMBIO)
	public ResponseEntity<?> create(@RequestBody TipoCambio tipoCambio, HttpServletRequest request) {
		try {
			TipoCambio tipoCambioResponse = tipoCambioService.saveLogic(tipoCambio, request);
			return new ResponseEntity<TipoCambio>(tipoCambioResponse, HttpStatus.OK);
		}catch (Exception e) {
			ErrorResponse ErrorResponse = new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getLocalizedMessage());
			return new ResponseEntity<ErrorResponse>(ErrorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping(Constantes.TIPOS_DE_CAMBIO + Constantes.TIPO_CAMBIO_KEY)
	public ResponseEntity<?> update(@PathVariable(value = "key") Long key, @RequestBody TipoCambio tipoCambio, HttpServletRequest request) {
		try {
			TipoCambio tipoCambioResponse = tipoCambioService.updateLogic(key, tipoCambio, request);
			return new ResponseEntity<TipoCambio>(tipoCambioResponse, HttpStatus.OK);
		}catch (Exception e) {
			ErrorResponse ErrorResponse = new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getLocalizedMessage());
			return new ResponseEntity<ErrorResponse>(ErrorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping(Constantes.TIPOS_DE_CAMBIO + Constantes.TIPO_CAMBIO_KEY)
	public ResponseEntity<?> update(@PathVariable(value = "key") Long key, HttpServletRequest request) throws Exception {
		try {
			tipoCambioService.deleteLogic(key, request);
			return new ResponseEntity<OkResponse>(new OkResponse(Constantes.MSG_ELIMINADO_OK), HttpStatus.OK);
		}catch (Exception e) {
			ErrorResponse ErrorResponse = new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getLocalizedMessage());
			return new ResponseEntity<ErrorResponse>(ErrorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
