package com.example.springboot.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.springboot.domain.Moneda;
import com.example.springboot.service.MonedaService;
import com.example.springboot.util.Constantes;

@RestController
@RequestMapping(Constantes.MONEDA)
public class MonedaController {

	@Autowired
	private MonedaService monedaService;

	@GetMapping(Constantes.MONEDA_CONSULTA)
    public ResponseEntity<List<Moneda>> monedas(){
		List<Moneda> monedas = monedaService.findAllActivos();
        return new ResponseEntity<List<Moneda>>(monedas, HttpStatus.OK);
    }
	
	@GetMapping(Constantes.MONEDA_CONSULTA_BY_SIMBOLO)
    public ResponseEntity<Moneda> monedas(@PathVariable("simbolo") String simbolo){
        return new ResponseEntity<Moneda>(monedaService.findBySimbolo(simbolo), HttpStatus.OK);
    }
	
}
