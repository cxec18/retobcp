package com.example.springboot.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.springboot.models.ErrorResponse;
import com.example.springboot.models.UsuarioRequest;
import com.example.springboot.models.UsuarioResponse;
import com.example.springboot.util.Constantes;
import com.example.springboot.util.JwtUtil;

@RestController
public class JwtAuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtUtil jwtTokenUtil;
    
    @RequestMapping(value = Constantes.AUTENTICACION, method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody UsuarioRequest usuarioRequest) throws Exception{
    	
    	ResponseEntity<?> respuesta = null;
    	String usuario = usuarioRequest.getUsuario();
    	String password = usuarioRequest.getPassword();
    	
    	
        try {
            Authentication authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(usuario, password));

            if (authenticate.isAuthenticated()) {
            	final UserDetails userDetails = (UserDetails)authenticate.getPrincipal();
                final String jwt = jwtTokenUtil.generateToken(userDetails);
                respuesta = ResponseEntity.ok(new UsuarioResponse(jwt));
            }

	    } catch (BadCredentialsException ex) {	    	
        	respuesta = handleMethodArgumentTypeMismatch(ex, "AUTENTICACION INCORRECTA");
        }catch (Exception ex) {
        	respuesta = handleMethodArgumentTypeMismatch(ex, ex.getMessage());
		}

        return respuesta;
    }

    
    public ResponseEntity<Object> handleMethodArgumentTypeMismatch(
    		Exception ex, String msgError) {
	    ErrorResponse errorResponse = 
	      new ErrorResponse(HttpStatus.UNAUTHORIZED, ex.getLocalizedMessage());
	    return new ResponseEntity<Object>(errorResponse, new HttpHeaders(), errorResponse.getStatus());
	}
}
