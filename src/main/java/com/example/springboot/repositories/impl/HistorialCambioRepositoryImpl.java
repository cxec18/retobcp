package com.example.springboot.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.example.springboot.domain.HistorialCambio;
import com.example.springboot.repositories.HistorialCambioRepositoryCustom;


@Repository
public class HistorialCambioRepositoryImpl implements HistorialCambioRepositoryCustom{

	    @PersistenceContext
	    EntityManager entityManager;

		@Override
		public List<HistorialCambio> customFindAll() {
			
			  return entityManager.createQuery(
			            "SELECT hc.* FROM " + HistorialCambio.class.getSimpleName() + " hc", 
			            HistorialCambio.class )
			        .getResultList();
	}
}
 