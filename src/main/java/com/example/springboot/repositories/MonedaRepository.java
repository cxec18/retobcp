package com.example.springboot.repositories;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.springboot.domain.Moneda;

@Repository
public interface MonedaRepository extends CrudRepository<Moneda, Serializable>{

    public static final String FIND_ONE_ACTIVO = 
                  "SELECT * FROM MONEDA WHERE "
                    + "SIMBOLO = :simbolo AND "
                    + "ACTIVO = True";

    public static final String FIND_ALL_ACTIVOS = 
                  "SELECT * FROM MONEDA WHERE activo=True";
    

    @Query(FIND_ONE_ACTIVO)
	public Moneda customFindBySimbolo(@Param("simbolo") String simbolo);

    @Query(FIND_ALL_ACTIVOS)
	public List<Moneda> customFindAllActivos();
    

}
