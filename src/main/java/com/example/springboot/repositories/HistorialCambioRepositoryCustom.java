package com.example.springboot.repositories;

import java.util.List;

import com.example.springboot.domain.HistorialCambio;

public interface HistorialCambioRepositoryCustom {

	public List<HistorialCambio> customFindAll();
}
