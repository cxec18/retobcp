package com.example.springboot.repositories;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.springboot.domain.TipoCambio;

@Repository
public interface TipoCambioRepository extends CrudRepository<TipoCambio, Serializable>{

    public static final String SEQUENSE = 
            "SELECT SQ_ID_TIPO_CAMBIO.nextval FROM dual";
    
    public static final String FIND_ONE_ACTIVO = 
            "SELECT * FROM TIPO_CAMBIO "
             + "WHERE "
                    + "TIPO_CAMBIO_KEY = :key AND "
                    + "ACTIVO = True";

    public static final String FIND_ONE_ORIGEN_DESTINO = 
            "SELECT * FROM TIPO_CAMBIO "
            + "WHERE "
                    + "SIMBOLO_MONEDA_ORIGEN = :simboloMonedaOrigen AND "
                    + "SIMBOLO_MONEDA_DESTINO = :simboloMonedaDestino AND "
                    + "ACTIVO = True";
    
    public static final String FIND_ALL_ACTIVOS = 
            "SELECT * FROM TIPO_CAMBIO "
            + "WHERE "
            		+ "ACTIVO=True";

    public static final String CUSTOM_SAVE = 
            "INSERT INTO TIPO_CAMBIO (TIPO_CAMBIO_KEY, SIMBOLO_MONEDA_ORIGEN, SIMBOLO_MONEDA_DESTINO, TASA_CONVERSION, USERNAME, FECHA, ACTIVO)"
           + "	VALUES (:key, "
			           + ":simboloMonedaOrigen, "
			           + ":simboloMonedaDestino, "
			           + ":tasaConversion, "
			           + ":username ,"
			           + ":fecha,"
			           + "true)";

    public static final String CUSTOM_UPDATE = 
            "UPDATE TIPO_CAMBIO "
		            + "SET "
		            + "SIMBOLO_MONEDA_ORIGEN = :simboloMonedaOrigen, "
		            + "SIMBOLO_MONEDA_DESTINO = :simboloMonedaDestino, "
		            + "TASA_CONVERSION = :tasaConversion, "
		            + "USERNAME = :username, "
		            + "FECHA = :fecha,"
		            + "ACTIVO = :activo "
            + "WHERE TIPO_CAMBIO_KEY = :key";
    
    
    @Query(SEQUENSE)
    public Long getNextSeriesId();

    @Query(FIND_ONE_ACTIVO)
	public TipoCambio customFindById(Long key);

    @Query(FIND_ONE_ORIGEN_DESTINO)
	public TipoCambio customFindByOrigenAndDestino(String simboloMonedaOrigen, String simboloMonedaDestino);

    @Query(FIND_ALL_ACTIVOS)
	public List<TipoCambio> customFindAllActivos();

    @Modifying
    @Query(CUSTOM_SAVE)
	public void customSave(Long key, String simboloMonedaOrigen, String simboloMonedaDestino,
			 double tasaConversion, String username, Date fecha);

    @Modifying
    @Query(CUSTOM_UPDATE)
	public void customUpdate(Long key, String simboloMonedaOrigen, String simboloMonedaDestino,
			 double tasaConversion, String username, Date fecha, boolean activo);
    

}
