package com.example.springboot.repositories;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.springboot.domain.HistorialCambio;

@Repository
public interface HistorialCambioRepository extends CrudRepository<HistorialCambio, Serializable>, HistorialCambioRepositoryCustom{


    public static final String SEQUENSE = 
            "SELECT SQ_ID_HISTORIAL_CAMBIO.nextval FROM dual";

    public static final String FIND_ONE = 
            "SELECT * FROM HISTORIAL_CAMBIO WHERE "
                    + "HISTORIAL_CAMBIO_KEY = :key ";

    public static final String FIND_ONE_BY_ID_TIPO_CAMBIO = 
            "SELECT * FROM HISTORIAL_CAMBIO JOIN TIPO_CAMBIO ON HISTORIAL_CAMBIO.TIPO_CAMBIO_KEY=TIPO_CAMBIO.TIPO_CAMBIO_KEY"
            		+ "WHERE "
                    + "TIPO_CAMBIO_KEY = :tipoCambioKey";

//    public static final String FIND_ALL = 
//            "SELECT * FROM HISTORIAL_CAMBIO as HC JOIN TIPO_CAMBIO as TP ON HC.TIPO_CAMBIO_KEY = TP.TIPO_CAMBIO_KEY";

    public static final String FIND_ALL = 
    "SELECT * FROM HISTORIAL_CAMBIO";

    public static final String CUSTOM_SAVE = 
            "INSERT INTO HISTORIAL_CAMBIO (HISTORIAL_CAMBIO_KEY, TIPO_CAMBIO_KEY, VALOR_MONEDA_ORIGEN, VALOR_MONEDA_DESTINO, FECHA, USERNAME)"
           + "	VALUES (:key, "
			           + ":idTipoCambio, "
			           + ":valorMonedaOrigen, "
			           + ":valorMonedaDestino, "
			           + ":fecha,"
			           + ":username)";

    public static final String CUSTOM_UPDATE = 
            "UPDATE HISTORIAL_CAMBIO "
		            + "SET "
		            + "TIPO_CAMBIO_KEY = :idTipoCambio, "
		            + "VALOR_MONEDA_ORIGEN = :valorMonedaOrigen, "
		            + "VALOR_MONEDA_DESTINO = :valorMonedaDestino, "
		            + "USERNAME = :username, "
		            + "FECHA = :fecha "
            + "WHERE HISTORIAL_CAMBIO_KEY = :key";
    
    
    @Query(SEQUENSE)
    public Long getNextSeriesId();

    @Query(FIND_ONE)
	public HistorialCambio customFindById(Long key);

    @Query(FIND_ONE_BY_ID_TIPO_CAMBIO)
	public List<HistorialCambio> customFindByIdTipoCambio(Long tipoCambioKey);

    @Modifying
    @Query(CUSTOM_SAVE)
	public void customSave(Long key, Long idTipoCambio, Double valorMonedaOrigen, Double valorMonedaDestino,
			  String username, Date fecha);

    @Modifying
    @Query(CUSTOM_UPDATE)
	public void customUpdate(Long key,Long idTipoCambio, Double valorMonedaOrigen, Double valorMonedaDestino,
			  String username, Date fecha);

    @Query(FIND_ALL)
	public List<HistorialCambio> customFindAllV2();
    

}
