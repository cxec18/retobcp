package com.example.springboot.repositories;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.springboot.domain.Usuario;

@Repository
public interface UsuarioRepository extends CrudRepository<Usuario, Serializable>{

    public static final String FIND_ONE_ACTIVO = 
                  "SELECT * FROM USUARIO WHERE "
                    + "USERNAME = :username AND "
                    + "ACTIVO = True";

    public static final String FIND_ALL_ACTIVOS= 
                  "SELECT * FROM USUARIO WHERE activo=True";

    @Query(FIND_ONE_ACTIVO)
	public Usuario customFindByUsername(String username);
    
    @Query(FIND_ALL_ACTIVOS)
	public List<Usuario> customFindAllActivos();

}
