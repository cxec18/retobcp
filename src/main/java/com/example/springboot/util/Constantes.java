package com.example.springboot.util;

public class Constantes {

    public static final String AUTENTICACION = "/authenticate";
    
    public static final String TIPO_CAMBIO = "/tipocambio";
    public static final String TIPOS_DE_CAMBIO = "/v1/tiposdecambio";
    public static final String TIPO_CAMBIO_ORIGEN_DESTINO = "/{origen}/{destino}";
    public static final String TIPO_CAMBIO_KEY = "/{key}";
    
    public static final String MONEDA = "/moneda";
    public static final String MONEDA_CONSULTA = "/v1/monedas";
    public static final String MONEDA_CONSULTA_BY_SIMBOLO = "/v1/monedas/{simbolo}";
    
    public static final String HISTORIAL_TIPO_CAMBIO = "/historialcambio";
    public static final String HISTORIALES_DE_TIPOS_DE_CAMBIO = "/v1/historiales";
    public static final String HISTORIALES_DE_TIPOS_DE_CAMBIO_V2 = "/v2/historiales";
    public static final String HISTORIAL_TIPO_CAMBIO_KEY = "/{key}";

    public static final String MSG_ELIMINADO_OK = "REGISTRO ELIMINADO";
    public static final String MSG_REGISTRO_NO_ENCONTRADO = "REGISTRO NO ENCONTRADO";
    public static final String MSG_BODY_INCORRECTO = "FORMATO INCORRECTO DEL BODY: FALTAN CAMPOS OBLIGATORIOS";
    public static final String MSG_TASA_INCORRECTA = "TASA INCORRECTA: DEBE SER MAYOR A 0";
    public static final String MSG_MONEDA_ORIGEN_INCORRECTO = "MONEDA ORIGEN NO ENCONTRADA";
    public static final String MSG_MONEDA_DESTINO_INCORRECTO = "MONEDA DESTINO NO ENCONTRADA";
    
    public static final String SECRET_KEY = "secret";
    public static final int DURACION_MINUTOS_KEY = 60;
}
